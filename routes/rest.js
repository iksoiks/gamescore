var express = require('express');
var router = express.Router();
var restify = require('express-restify-mongoose');
var Match = require('../models/match');
var Player = require('../models/player');
var Tournament = require('../models/tournament');

restify.serve(router, Match);
restify.serve(router, Player);
restify.serve(router, Tournament);

module.exports = router;
