var mongoose = require('mongoose');

var PlayerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  }
});

var Player = mongoose.model('Player', PlayerSchema);

module.exports = Player;
