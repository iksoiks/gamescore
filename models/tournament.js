var mongoose = require('mongoose');

var TournamentSchema = new mongoose.Schema({
  date: {
    type: Date,
    default: Date.now
  },
  game: {
    type: String,
    required: true
  },
  gameType: {
    type: String,
    required: true
  },
  players: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Player'
  }],
  status: {
    type: Number,
    default: 0
  },
  matches: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Match'
  }],
  winnerPlayers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Player'
  }]
});

var Tournament = mongoose.model('Tournament', TournamentSchema);

module.exports = Tournament;
