var mongoose = require('mongoose');

var MatchSchema = new mongoose.Schema({
  date: {
    type: Date,
    default: Date.now
  },
  game: {
    type: String,
    required: true
  },
  gameType: {
    type: String,
    required: true
  },
  players: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Player'
  }],
  score: [{
    type: Number
  }]
});

var Match = mongoose.model('Match', MatchSchema);

module.exports = Match;
